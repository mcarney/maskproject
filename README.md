# Mask Project

We aim to mass produced using traditional manufacturing proceses a resuable, sterilizable, filter-media agnostic N95 alternative half-face respirator.

We have identified an ideal candidate filter media working with https://www.hollingsworth-vose.com/ that meets spec for our given cross-sectional area. 



Main progress is being tracked inside a private (private in order to manage Signal/Noise ratio) slack channel on http://helpfulengineering.org

A little more info can be found at https://maskproject.tech

## 2020-05-25
We have established a 501(c)3 non-profit [Open Standard Respirator, Inc.](https://www.openstandardrespirator.org) to house the design specifications of the OSR Model 1 device.

A new mechanical interlock is now in use to secure the face-piece to the rigid filter adapter. To verify the design a segment of the device was prototyped by printing rigid adapter and interlock components and casting the gasket segment of the face-piece. 
![alt text](media/interlockTests.jpg "Interlock testing.")

Our partners in Portugal at Simmoldes are now running first article tests on multi-cavity molds. Family mold with two full sets of rigid components. Simmoldes is a Tier 1 automotive manufacturer who has been involved with the development of this design.
![alt text](media/rigidAdapterMolding.jpg "Family mold with two full sets of rigid components.") 



Filters continue to be a bit of an issue. We had identified ideal filter material that passed all tests but then was entirely purchased by the DoD for ventilators. Alternative nanofiber materials are nearly promising, though their pressure drop is a bit high. Technically they pass NIOSH tests but are difficult for a user. We are actively engaging alternative manufacturers.  

Please contact us mcarney@media.mit.edu if you have filter contacts. 

Finally, we are preparing for a field study with 200 users at three different North Carolina county EMS teams. This should occur once we receive first molded parts off the tools.

## 2020-04-13
First tooled component from our prototype LSR injection molding facility in OR [at Kaos Softwear](https://www.kaossoftwear.com/). This is a molded face-mask portion, and 3d printed rigid components.

![alt text](media/inUse.jpg "Molded face-mask portion, 3d printed rigid components.")

Cutting molds for the LSR face-mask portion.

![alt text](media/horizontalMilling.jpg "Horizontal mill for prototype mold bases.")

First tooled components from Protolabs should be shipping today.

We are testing filter material at four different particulate testing facilities: CBA, MIT Chem. Eng. Smith College, Wakeforest University.  An important consideration is that air velocity changes kinetic energy of particles which may effect filter efficiency. Also, non-woven media relies on electrostatic charges that may act differently with different particle types: NaCl will be havior differently from Au or Latex spheres, or incense smoke.  These are all good particles to test, but likely to produce different results than the NaCl specified in the N95 specifications.

We are also coordinating with some Media Lab member companies that can do bacterial challenges to the filters.

In order to better understand air velocity and residual CO2 in the masks we are moving forward with CFD analysis.  Suchit Jain from Solidworks has been helping us get moving on this.  We are starting with an analysis that has been tested on prototype hardware already such that we can validate our simulation.  Once simulation aligns reasonably well with collected data we can assume it the filter media and boundary conditions are reasonablly appropriate to be used for design iterations.

The top frame is CO2, the bottom frame is air velocity.

[![alt text](media/simulation_01.png)](https://youtu.be/Oq5m01PZXtI)


## 2020-04-07
More prototypes sent to NIOSH testing facility. These are liquid silicone 3D printed face mask and Connex 500 printed filter media. The Filter media is H&V TSP100NS015

We started cutting molds today.

Some sneak peaks of what's coming:
![alt text](media/FM04-A001_FaceMaskAssy_2020-04-07_back.png "Assembled FM04-A001_FaceMaskAssy")

re: Filters We have preliminary data from consultation from H&V engineers, but verification at higher air velocities is necessary.  If data already exists that'd be great to use! If not, I've got material sheets and would love to get these samples to you for testing, asap!

H&V filter materials. [TSP100NS015](https://drive.google.com/open?id=1u_pYODF5bBaUW1ejN6_HwFc1GDGVjMfi), TSP050YA002, TS050 Plus, PN13015AP2 (datasheets available upon request, they seem to not want them publicly out there)

Specific ASTM testing results/needs requested for our effort :

*  Bacterial Filtration Effıciency—Determine the bacterial Test Method **F2101**
*  Sub-Micron Particulate Filtration—Determine particulate filtration efficiency as directed in Test Method **F2299**. Applied at 35cm/s air velocity.
*  Resistance to Penetration by Synthetic Blood Determine synthetic blood penetration resistance as specified in Test Method **F1862**.
*  Flammability—Determine flammability as specified in **16 CFR Part 1610**.


Our MASKproject.tech is in line for testing at MGH, though validating filter data sooner than later would really expedite fixing design challenges. We've got 200 masks incoming Friday from the injection molders and have NIOSH ongoing, but the ASTM filtration validation at higher velocity is really important to validate efficacy.  


## 2020-04-06
We are cutting tools on Monday (2020-04-06) and shipping first articles to Atorlabs in FL for NIOSH testing, Wakeforest Baptist Hospital in NC for particle testing.  If those pass we will then forward samples to MGH for final testing and verification.

Likely there will be a design update prior to final scaleup.


## 2020-04-05
Here is testing at a NIOSH testing facility from over the weekend. The face mask shown is already outdated.

![alt text](media/testingNIOSH.jpg "Testing to NIOSH standards")

## 2020-04-01
A quick update of progress from 2020-04-01 can be found [here](https://docs.google.com/presentation/d/1jY9FE1if-1H9EG4_PTsXMSJp7RggyXXnRyWPLdJphJE/edit?usp=sharing)
